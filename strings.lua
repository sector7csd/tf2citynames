﻿function data()
	info_name = "Placerholder_Info_Name"
	info_desc = "Placerholder_Info_Desc"
	name_ttd = "Placerholder_name_ttd"

	return {
		de = {
			[info_name] = ("Transport Tycoon Deluxe Städte und Gemeinden Namen"),
			[info_desc] = (
				       "Es leuchtet blau"
			),

			[name_ttd] = "Deutschland - Transport Tycoon Deluxe like",
		},
		en = {
			[info_name] = ("Transport Tycoon Deluxe cities and municipalities names"),
			[info_desc] = (
					"It glow's blue"
			),

			[name_ttd] = "Germany - Transport Tycoon Deluxe like",
		},
	}
end
