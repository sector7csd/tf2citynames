# TF2 Citynames

## Installation

Clone this git repo into the staging_area of the game, e.g **~/.local/share/Steam/userdata/<your steamid>/1066780/local/staging_area**

The directory has to end with a **"_1"**, be sure to clone to like this:

```bash
git clone git@bitbucket.org:sector7csd/tf2citynames.git tf2citynames_1
```
