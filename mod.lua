require "tableutil"

function data()
return {
	info = {
	minorVersion = 2,
	severityAdd = "NONE",
	severityRemove = "NONE",
	name = _(info_name),
	description = _(info_desc),
	tags = {"Europe", "Misc"},
	},
	categories = {
		{ key = "nameList", name = _("Town names") },
	},
	options = {
		nameList = {
            {"de_ttd", _(name_ttd)},
        },
    }
}
end
