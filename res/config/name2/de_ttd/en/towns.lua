require "tableutil"

function data()

	local startNames = {	"Ham",  		"Els",  		"Vor",  		"Post",   		"Delmen", 		"Olden", 		"Cloppen", 		"Quaken", 		"Dink", 		"Hann",
							"Osna", 		"Diep",  		"Tarm",  		"Wester",  		"Kirch",  		"Oster",  		"Wildes", 		"Groß",  		"Bruns", 		"Gnarren",
							"Norden", 		"Cux", 			"Bremer", 		"Hep", 			"Bred",			"Mühlen",		"Schnacken",	"Roten",		"Garten",		"Hell",
							"Sonders",		"Golden",		"Hees",			"Lauen",		"To",			"Lengen",		"Sitten",		"Hamer",		"Hollen",		"Heide",
							"Güters",		"Neuen",		"Walds",		"Peters",		"Lem",			"Lilien",		"Gras",			"Hütten",		"Hunter",		"Hol",
							"Ketten",		"Bad",			"Herz",			"Düssel",		"Duis",			"Frank",		"Stutt",		"Hildes",		"Alm",			"Wehl",
							"Brock",		"Hese",			"Mahn",			"Katten",		"Wil",			"Brütten",		"Bade",			"Stein",		"Fisch",		"Hor",
							"Mulms",		"Doden",		"Giers",		"Badener",		"Aller",		"Huckel",		"Harp",			"Ehren",		"Dort",			"Alten",
							"Schöne",		"Frohn",		"Berger",		"Glad",			"Pader",		"Biele",		"Harse",		"Franken",		"Toppen",		"Lüne",
							"Ratze",		"Branden",		"Nord",			"Langen",		"Wupper",		"Madge",		"Kaisers",		"Saar",			"Schwein",		"Lever",
							"Mar"
					 	 }

	local endNames   = {	"burg", 		"dorf", 		"werk", 		"hausen", 		"horst",  		"brück", 		"lage",    		"hof",    		"holz", 		"over",
		 					"beck", 		"stedt", 		"timke", 		"kirchen", 		"kneten", 		"büttel", 		"ham",    		"haven", 		"land",  		"vörde",
							"bergen", 		"singen",		"tal",			"bach",			"wege",			"wedel",		"rade",			"bergen",		"hagen",		"lingen",
							"sen",			"bostel",		"loh",			"heide",		"rode",			"feld",			"kamp",			"horn",			"heim",			"wald",
							"schloß",		"moor",			"förde",		"mühlen",		"winkel",		"marschen",		"furt",			"gart",			"busch",		"damm",
							"turm",			"höfen",		"riede",		"walde",		"bruch",		"scheid",		"born",			"falen",		"hütte",		"höhe",
							"mold",			"nau",			"neben",		"schacht",		"pommern",		"dal",			"lautern",		"bronn",		"brücken",		"kusen"
					 	 }

	local names      = {}

	for startIndex, startValue in ipairs(startNames) do
		for endIndex, endValue in ipairs(endNames) do
			local name  = startValue .. endValue

			table.insert(names, name)
		end
	end

	return names

end
